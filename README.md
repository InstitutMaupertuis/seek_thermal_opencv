[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

This example displays the RAW image (not the thermography image)

# Dependencies
- [SeekWare SDK](https://developer.thermal.com/)

# Output images
- Output images are grayscale 16 bit images.
- Seek Thermal pixels are coded on 16 bits (0 to 65 535).

Use a lighter in front of the camera to see something: the image is very dull without rescaling values.
