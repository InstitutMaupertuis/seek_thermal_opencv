#include <seekware/seekware.h>
#include <signal.h>
#include <string>
#include <iostream>
#include <opencv/cv.h>
#include <opencv2/highgui.hpp>

bool exit_requested(false);

void print_fw_info(psw camera)
{
  sw_retcode status;
  int therm_ver;

  std::cout << "Model Number: " << camera->modelNumber << std::endl;
  std::cout << "SerialNumber: " << camera->serialNumber << std::endl;
  std::cout << "Manufacture Date: "<<  camera->manufactureDate << std::endl;
  std::cout << "Firmware Version: " << (unsigned) camera->fw_version_major << "." << (unsigned) camera->fw_version_minor
                                    << (unsigned) camera->fw_build_major << "." << (unsigned) camera->fw_build_minor << std::endl;

  status = Seekware_GetSettingEx(camera, SETTING_THERMOGRAPHY_VERSION, &therm_ver, sizeof(therm_ver));
  if (status != SW_RETCODE_NONE)
  {
    throw std::runtime_error("Error: Seek GetSetting returned " + std::to_string(status));

  }
  std::cout << "Themography Version: " << therm_ver << std::endl;

  sw_sdk_info sdk_info;
  Seekware_GetSdkInfo(nullptr, &sdk_info);
  std::cout << "Image Processing Version: " << (unsigned) sdk_info.lib_version_major << "." << (unsigned) sdk_info.lib_version_minor
            << "." << (unsigned) sdk_info.lib_build_major << "." << (unsigned) sdk_info.lib_build_minor << std::endl;
}

void signal_callback(int)
{
  std::cout << "Exit requested!" << std::endl;
  exit_requested = true;
}

void cleanup(psw camera, uint16_t *data)
{
  std::cout << "Exiting..." << std::endl;
  if (camera != nullptr)
    Seekware_Close(camera);

  if (data != nullptr)
    delete[] data;
}

int main(int, char **)
{
  uint64_t frame_count(0);
  size_t camera_pixels(0);
  sw_retcode status;
  psw camera(nullptr);
  psw camera_list[1]; // Expect only one camera

  uint16_t *filtered_data = nullptr;

  signal(SIGINT, signal_callback);
  signal(SIGTERM, signal_callback);

  std::cout << "seekware-simple - A simple data capture utility for Seek Thermal cameras" << std::endl;

  sw_sdk_info sdk_info;
  Seekware_GetSdkInfo(nullptr, &sdk_info);
  std::cout << "SDK Version: " << (unsigned) sdk_info.sdk_version_major << "." << (unsigned) sdk_info.sdk_version_minor << std::endl;

  int num_cameras_found(0);
  status = Seekware_Find(camera_list, 1, &num_cameras_found);
  if (status != SW_RETCODE_NONE || num_cameras_found == 0)
  {
    std::cout << "Cannot find any cameras...exiting" << std::endl;
    return 1;
  }

  camera = camera_list[0];
  status = Seekware_Open(camera);
  if (status != SW_RETCODE_NONE)
  {
    std::cerr << "Cannot open camera: " << status << std::endl;
    cleanup(camera, filtered_data);
    return 1;
  }

  // Must read firmware info AFTER the camera has been opened
  std::cout << "Camera firmware info: " << std::endl;
  print_fw_info(camera);

  camera_pixels = (size_t)camera->frame_cols * (size_t)camera->frame_rows;

  filtered_data = new uint16_t[camera_pixels + camera->frame_cols];
  if (filtered_data == nullptr)
  {
    std::cerr << "Cannot allocate filtered buffer!" << std::endl;
    cleanup(camera, filtered_data);
    return 1;
  }

  uint32_t enable (1);
  Seekware_SetSettingEx(camera, SETTING_ENABLE_TIMESTAMP, &enable, sizeof(enable));
  Seekware_SetSettingEx(camera, SETTING_RESET_TIMESTAMP, &enable, sizeof(enable));

  cv::namedWindow("Seek Thermal", cv::WINDOW_KEEPRATIO);
  while (!exit_requested)
  {
    status = Seekware_GetImageEx(camera, filtered_data, nullptr, nullptr);
    if (status == SW_RETCODE_NOFRAME)
    {
      std::cout << "Seek Camera Timeout ..." << std::endl;
      continue;
    }
    if (status == SW_RETCODE_DISCONNECTED)
    {
      std::cout << "Seek Camera Disconnected ..." << std::endl;
      continue;
    }
    if (status != SW_RETCODE_NONE)
    {
      std::cout << "Seek Camera Error: " << status << std::endl;
      break;
    }

    ++frame_count;
    std::cout << "Frame Info:" << std::endl;
    std::cout << "frame_width: " << camera->frame_cols << std::endl;
    std::cout << "field_height: " << camera->frame_rows << std::endl;
    std::cout << "frame_count: " << frame_count << std::endl;

    cv::Mat mat(camera->frame_rows, camera->frame_cols, CV_16UC1, filtered_data);
    cv::imshow("Seek Thermal", mat);
    cv::waitKey(1);
  }

  cleanup(camera, filtered_data);
  return 0;
}
